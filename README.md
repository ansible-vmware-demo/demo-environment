# demo-environment



## Getting started

This project deploys an integration demo between Ansible Automation Platform and VMware vCenter


## Requirements

### Collections:
  - [ansible.controller](https://console.redhat.com/ansible/automation-hub/repo/published/ansible/controller/)

    (Can be replaced by the equivalent version of [awx.awx](https://galaxy.ansible.com/ui/repo/published/awx/awx/). Tested with 23.9.0
    You'd need to change the module names by the awx ones in the file `configure-ansible-controller.yml`.
### Ansible Core
  - Tested with ansible core 2.16.2

## Configuration

Copy the vars template located in the `vars` directory `ansible-controller-objects-vars.yml.template`to a new file called `ansible-controller-objects-vars.yml` and add your own variables there. 

This filename `ansible-controller-objects-vars.yml` is referenced in `.gitignore`. Keep this file out of the repo or your sensitive data could be exposed.

## Usage

Execute `ansible-playbook -i inventory deploy.yml` from the root directory of the project. 

It should create every needed object in the Ansible Controller.
